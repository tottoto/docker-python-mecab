from python:3.7.0
LABEL maintainer="tottoto <tottotodev@gmail.com>"

RUN apt-get update -y && \
    apt-get install -y --no-install-recommends \
        mecab \
        mecab-ipadic-utf8 \
        libmecab-dev && \
    apt-get clean && \
    rm -rf /var/cache/apt/archives/* /var/lib/apt/lists/*

RUN pip install pipenv
